
public abstract class Shape {
	
	public abstract double getArea(); 
	
	public double addArea(Shape s) {
		return this.getArea() + s.getArea();
	}

}
