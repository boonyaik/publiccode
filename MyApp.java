
public class MyApp {
	
	public static void main(String[] args) {
		
		SavingAccount sa1 = new SavingAccount("James",200);	
		SavingAccount sa2 = new SavingAccount("Jane",800);
		//sa2.transfer(sa1,100);
		System.out.println(sa2.checkBalance()); //-900
		
		CurrentAccount ac1 = new CurrentAccount("John",100);
		//ac1.transfer(sa2, 1000);
		System.out.println(sa2.checkBalance()); 
		
		ForeignAccount fa = new ForeignAccount("Ali",1000);
		//fa.transfer(ac1, 100);
		System.out.println(fa.checkBalance());
	}
}
