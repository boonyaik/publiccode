
public class RaceTrack {
	
	Racer p1;
	Racer p2;
	
	public RaceTrack(Racer p1, Racer p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	public void startRace() {
		p1.move();
		p2.move();
	}

}
