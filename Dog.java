
public class Dog {

	String color; 
	
	public Dog(String color) {
		this.color = color;
	}
	
	public String getColor() {
		return this.color;
	}
	
	@Override 
	public int hashCode() {
		int total = 0; 
		for ( int i = 0; i<color.length() ; i++) {
			total = total + (int)color.charAt(i);
		}
		return total;
	}
	
	@Override 
	public boolean equals(Object o) {
		if (this.color == ((Dog)o).color) {
			return true;
		}return false;
	}

}
