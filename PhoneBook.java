import java.util.ArrayList;
import java.util.List;

public class PhoneBook {
	List<Contact> contacts = new ArrayList<>();
	
	public PhoneBook(int size) {
		//contacts = new Contact[size];
	}
	
	public void addContact(String name, String number) {
		contacts.add(new Contact(name,number));
	}
	
	public String getNumber(String name) {
		for ( int i = 0 ; i < contacts.size() ; i++) 
		{
			if ( contacts.get(i).getName() == name.intern()) {
				return contacts.get(i).getNumber();
			}
		}
		return null;
	}
	
	public void listOut() {
		for ( int i = 0 ; i < contacts.size()  ; i++) {
			System.out.println(contacts.get(i).getName() + " " + contacts.get(i).getNumber());
		}
	}
	
}
