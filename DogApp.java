import java.util.HashMap;
import java.util.Map;

public class DogApp {

	public static void main(String[] args) {
		
		Dog a = new Dog("Red");
		Dog c = new Dog("Brown");
	
		System.out.println(a.hashCode());
		System.out.println(c.hashCode());
		
		Map<Dog,Integer> hmap = new HashMap<>();
		hmap.put(a, 10);
		hmap.put(c, 11);
		a = null;
		
		Dog b = new Dog("Red");
		System.out.println(hmap.get(b));

	}

}
