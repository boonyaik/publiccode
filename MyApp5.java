import java.util.LinkedList;
import java.util.Queue;

public class MyApp5 {
	
	public static void main(String args[]) {
		
		Queue <Integer> myQ = new LinkedList<>();
		myQ.add(2);
		myQ.add(3);
		myQ.add(6);
		myQ.add(7);
		
		System.out.println(myQ.poll());
		System.out.println(myQ.poll());
		System.out.println(myQ.poll());
	}

}
