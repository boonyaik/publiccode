
public class SavingAccount extends BankAccount{
	
	public SavingAccount(String bank_no, int balance) {
		super(bank_no, balance);
	}

	public boolean withdraw(int amount) throws Exception {
		if (this.checkBalance() - amount <= 10) {
			//do nothing
			//System.out.println("Not Enough Balance");
			throw new Exception();
			//return false;
		}else {
			super.withdraw(amount);
			return true;
		}
	}
	

	

}
