
public class Contact {
	private String name;
	private String number;	
	//constructor
	public Contact(String input_name, String input_number) {
		this.name = input_name; 
		this.number = input_number;
	}
	public String getName() {
		return name;
	}
	public String getNumber() {
		return number;
	}
}
