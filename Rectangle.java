public class Rectangle extends Shape{

	private double x;
	private double y; 
	
	
	public Rectangle(double x, double y) {
		setX(x);
		setY(y);
	}
	public Rectangle(int x, int y) {
		setX(x);
		setY(y);
	}
	//function overloading 
	public Rectangle(Rectangle r) {
		setX(r.x);
		setY(r.y);
	}
	
	public void setX(double x) {
		if ( x < 1) { 
			this.x = 1;
		}else{
			this.x = x;
		}
	} 
	public void setY(double y) {
		if ( y < 1) { 
			this.y = 1;
		}else{
			this.y = y;
		}
	} 
	
	public double getArea() {
		return this.x * this.y;
	}
	
	double getPerimeter() {
		return 2*this.x + 2*this.y;
	}
	double getDiagonal() {
		return this.x*this.x + this.y*this.y;
	}
	
	public static void hello() {
		
	}
	
	public static void main(String[] args) {
//		Rectangle r1 = new Rectangle();
//		r1.x = 5 ;
//		r1.y = 10;
//		System.out.println(r1.getArea());
		
	}

}
