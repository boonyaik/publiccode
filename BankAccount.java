
public class BankAccount {
	
	private String bank_no; 
	private int balance; 
	
	public BankAccount(String bank_no, int balance) {
		this.bank_no = bank_no;
		this.balance = balance;
	}
	
	public void deposit(int amount) {
		balance += amount;
	}
	
	public boolean withdraw(int amount) throws Exception {
		balance -= amount;
		return true;
	}
	
	public int checkBalance() {
		return this.balance;
	}
	
	public void transfer(BankAccount destination, int amount) throws Exception {
		if( this.withdraw(amount) == true) {
			destination.deposit(amount);
		}
	}
}
