
public class Test {
	int my_instance_x; 
	
	public Test(int x) {
		this.my_instance_x = x ;
	}
	
	public Test() {
		this(0);
	}
	
	public static void main(String[] args) {
		{
		long start = System.currentTimeMillis();
		String a = "t";
		for ( int i = 0 ; i < 5000; i++) {
			a = a + "Hello" + "World" + "This" + "is" + "Nice";
		}
		long stop = System.currentTimeMillis();
		System.out.println(stop-start);
		}
		
		{
		long start = System.currentTimeMillis();
		StringBuffer a = new StringBuffer("t");
		for ( int i = 0 ; i < 5000; i++) {
			a.append("Hello");
			a.append("World");
			a.append("This");
			a.append("is");
		}
		long stop = System.currentTimeMillis();
		System.out.println(stop-start);
		}
	}

}
