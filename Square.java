
public class Square extends Shape{
	
	double x; 
	
	public Square(int x) {
		this.x = x; 
	}
	
	public double getArea() {
		return x*x; 
	}

}
