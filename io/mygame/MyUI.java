package io.mygame;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import io.alex.StopWatch;

public class MyUI extends JFrame{

	JButton myButton;
	JButton myButton2;
	JLabel mylbl; 
	Thread th ;
	
	public MyUI() {
		super("My First Frame");
		setSize(400,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setLayout(new FlowLayout());
		myButton = new JButton("Start");
		myButton2 = new JButton("Press me 2 !");
		mylbl = new JLabel();
		StopWatch sw = new StopWatch();
	
		myButton.addActionListener(e -> { //lambda 
			System.out.println("Game On");	
			mylbl.setText("Game On");
			myButton2.setBackground(null);
			if (th != null) {
				System.out.println("not null");
				if(th.isAlive()) {
					th.interrupt();
				}
			}
			th = new Thread(new Runnable() {
				public void run() {
					Random rand = new Random();
					try {
						Thread.sleep((rand.nextInt(6)+2)*1000);
						myButton2.setBackground(Color.GREEN);
						sw.start();
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}	
				}
			});
			th.start();
			
			
		});
		
		myButton2.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					if (th.isAlive() == true) {
						System.out.println("Let's try again!");
						mylbl.setText("Let's try again!");
						th.interrupt();
					}else {
						sw.stop();
						System.out.println(sw.getDuration());
						mylbl.setText("GG : " + sw.getDuration() + "ms");
						myButton2.setBackground(null);
					}
					
				}catch (Exception ex){
					JOptionPane.showMessageDialog(null, "Press Start First");
				}
			}
		});
		add(myButton);
		add(myButton2);
		add(mylbl);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		MyUI m = new MyUI();

	}

	

}
