package io.alex;

public class Lapse {

	StopWatch[] swatches = new StopWatch[100];
	int count = 0;
	
	public void start() {
		swatches[count] = new StopWatch();
		swatches[count].start();
	}
	
	public void stop() throws Exception {
		swatches[count].stop();
		this.count++;
	}
	
	public long getDuration(int lap) {
		return swatches[lap].getDuration();
	}
	
	public long getTotalDuration() {
		long sum = 0; 
		for ( int i = 0; i < count ; i++) {
			sum = sum + swatches[i].getDuration();
		}
		return sum;
	}
	
}
