package io.alex;

public class StopWatch {
	private long start = 0;
	private long stop = 0; 
	
	public long getDuration() {
		return stop - start; 
	}
	
	public void start() {
		start = System.currentTimeMillis();
	}
	
	public void stop() throws Exception {
		if (start == 0) {
			throw new Exception();
		}
		stop = System.currentTimeMillis();
	}
	
	public void reset() {
		start = stop = 0;
	}
}
