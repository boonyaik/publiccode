
public class Car {

	int numberOfDoors;
	double engine_Displacement; 
	private int speed;
	static int x;
	
	public void setSpeed(int speed) {
		if (speed <= 0) {
			this.speed = 0;
		}else{
			this.speed = speed;
		}
	}
	
	public static void main(String[] args) {
		Car myCar = new Car();
		//myCar.speed = -100; cannot compile
		myCar.numberOfDoors = 4;
		Car.x = 10;
		
		
		System.out.println(myCar.numberOfDoors);
		
		Car yourCar = new Car();
		yourCar.numberOfDoors = 2;
		System.out.println(yourCar.numberOfDoors);
		

	}

}
