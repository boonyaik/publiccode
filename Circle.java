
public class Circle extends Shape implements Racer{

	double r = 0; 
	public Circle(double r) {
		this.r = r;
	}
	
	@Override
	public double getArea() {
		return 3.142*r*r;
	}
	@Override
	public void move() {
		// TODO Auto-generated method stub
		
	}

}
