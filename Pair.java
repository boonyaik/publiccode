
public class Pair<T,S> {
	private T firstItem; 
	private S secondItem;
	public T getFirstItem() {
		return firstItem;
	}
	public S getSecondItem() {
		return secondItem;
	}
	public Pair (T firstItem, S secondItem) {
		this.firstItem = firstItem;
		this.secondItem = secondItem;
	}
}
