
public class App3 {
		
	public static void main(String args[]) {
		
		Pair<Integer,Integer> x = new Pair<>(new Integer(5), new Integer(7));
		System.out.println(x.getFirstItem() + " " + x.getSecondItem());
		
		Pair<String, Integer> y = new Pair<>("Five", new Integer(5));
		System.out.println(y.getFirstItem() + " " + y.getSecondItem());
	}
}
